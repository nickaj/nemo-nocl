.. _SSHFS:

************
SSHFS & FUSE
************

*"This is a filesystem client based on the SSH File Transfer Protocol. Since most SSH servers already support this protocol it is very easy to set up: i.e. on the server side there's nothing to do”*

Why use sshfs at NOCL?

If you need access to large data sets on remote file systems, using sshfs will allow do this without having to download the data locally.

How do I use sshfs at NOCL?

* First you will need a user id on the remote file system.
* Password-less login may also been helpful here :kbd:`man ssh-keygen`
* Check your system has fuse and sshfs available :kbd:`which fusermount sshfs`
* Create an empty directory to provide the mount point for the remote filesystem.
* Issue the command :kbd:`sshfs path_to_remote_dir path_to_mount_point -o default_permissions,uid=XXXX,gid=YYYY,umask=022'`. XXXX and YYYY are your user id and group id (see below).
* To unmount the remote filesystem issue the command :kbd:`fusermount -u path_to_mount_point`

A simple example to illustrate:

In my :kbd:`.bashrc` I've set up an alias to mount and an alias to unmount the remote hector filesystem.

.. code-block:: bash

    alias mnt_rmt='sshfs jdha@supercomputer.ac.uk:/work /work/jdha/mount_points/supercomuter -o default_permissions,uid=19758, gid=18720, umask=022'
    alias umnt_rmt='fusermount -u /work/jdha/mount_points/supercomputer'

In mounting the filesystem you have to pass your user id (:kbd:`uid`) and group id (:kbd:`gid`). If you type :kbd:`id` on the command line you will be able to identify your :kbd:`uid` and :kbd:`gid`. You will then have to request that your :kbd:`uid` is added to the fusergroup (contact NOCL help desk).

The directory :kbd:`/work/jdha/mount_points/supercomputer` is an empty directory that is created for the sole purpose of mounting the remote filesystem and should not be populated.

Points to note:

1. Do not locate your mount points on :kbd:`/login` - if the remote filesystem is large. Any requests to the remote filesystem, even a basic ls, will grind things to a halt. This happens as :kbd:`/login` is not part of GPFS and cannot handle the overhead. I would suggest providing empty mount points on :kbd:`/work`.
2. You can use ssh passwordless login by generating ssh keys for local and remote sites to get around being prompted for your password each time you want to mount the remote filesystem.
3. If the filesystem you are trying to access happens to be behind a *login* machine you will first have to tunnel through to provide access:
  * first set up the tunnel: :kbd:`ssh -f login.machine.ac.uk -L 2221:remote_machine_name:22 -N`
  * then mount as before: :kbd:`sshfs -p 2221 -o transform_symlinks localhost:/ /work/jdha/mount_points/my_mount_point/`. The choice of port number is arbitrary.
