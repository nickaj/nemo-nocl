.. _ESGFSummary:

*******************************
CMIP5 ocnBgchem Summary (RCP85)
*******************************

=============  ================  ====  ====  ====  ====
group          model             no3   si    o2    po4
=============  ================  ====  ====  ====  ====
BCC            bcc-csm1-1-m     
BCC            bcc-csm1-1      
BNU            BNU-ESM                       yr    yr
CCCma          CanESM2           mn    
CMCC           CMCC-CESM         mn          mn    mn
CNRM-CERFACS   CNRM-CM5          mn    mn    mn    mn
INM            inmcm4           
IPSL           IPSL-CM5A-LR      mn    mn    mn    mn
IPSL           IPSL-CM5A-MR      mn    mn    mn    mn
IPSL           IPSL-CM5B-LR      mn    mn    mn    mn
MIROC          MIROC-ESM-CHEM    yr
MIROC          MIROC-ESM         yr
MOHC           HadGEM2-CC        mn    mn    mn    
MOHC           HadGEM2-ES        mn    mn    mn   
MPI-M          MPI-ESM-LR        mn    mn    mn    mn
MPI-M          MPI-ESM-MR        mn    mn    mn    mn
NASA-GISS      GISS-E2-H-CC      mn    mn    
NASA-GISS      GISS-E2-R-CC      mn    mn    
NCC            NorESM1-ME        mn    mn    mn    mn
NOAA-GFDL      GFDL-ESM2G        mn    mn    mn    mn
NOAA-GFDL      GFDL-ESM2M        mn    mn    mn    mn
NSF-DOE-NCAR   CESM1-BGC         mn    mn    mn    mn
=============  ================  ====  ====  ====  ====

.. note:: Only the first ensemble has been used (r1i1p1). Do 
          we want to use all the ensemble members?
